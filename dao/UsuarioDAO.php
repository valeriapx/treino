<?php
require_once '../vo/Usuario.php';
require_once '../generic/BDPO.php';
class UsuarioDAO {
    public static $instance;
    private function __construct() {}
    
    public static function getInstance(){
        if(!isset(self::$instqance))
            self::$instance = new UsuarioDAO ();
        return self::$instance;
    }
    
    public function insert(Usuario $usuario){
        try{
            $sql = "INSERT INTO usuario(nome,email,senha,tipo)"
                    . "VALUES (:nome, :email, :senha, :tipo)";
            $p_sql = BDPDO::getInstance()->prepare(sql);
            
            $p_sql->bindValue(":nome", $usuario->getNome());
            $p_sql->bindValue(":email", $usuario->getEmail());
            $p_sql->bindValue(":tipo", $usuario->getTipo());
            //iremos criptografar a senha para md5, assim o usuário terá
            //mais segurança, já que frequentimente usamos a mesma senha
            //para diversas aplicações.
            $p_sql->bindValue(":senha", md5($usuario->getSenha()));
            return $p_sql->execute();
        } catch (Exception $e){
            print "Erro ao executar a função de salvar".$e->getMessage();
        }
    }
}
