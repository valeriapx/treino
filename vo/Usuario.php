<?php
class Usuario {
    private $id = 0;
    private $nome;
    private $email;
    private $senha;
    private $tipo;
    
    function getId() {
        return $this->id;
    }

    function setId($id): void {
        $this->id = $id;
    }

    function getNome(){
        return $this->nome;
    }
    
    function setNome($nome){
        $this->nome = $nome;
    }
    
    function getEmail() {
        return $this->email;
    }

    function setEmail($email): void {
        $this->email = $email;
    }

    function getSenha() {
        return $this->senha;
    }

    function setSenha($senha): void {
        $this->senha = $senha;
    }

    function getTipo() {
        return $this->tipo;
    }

    function setTipo($tipo): void {
        $this->tipo = $tipo;
    }

}
